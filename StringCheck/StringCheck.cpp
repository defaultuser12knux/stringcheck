﻿#include <iostream>
#include <string>

void main()
{
	std::string StringCheck = "Word";
	std::cout << "The word is: " << StringCheck << std::endl;
	std::cout << "Word length: " << StringCheck.length() << std::endl;
	std::cout << "First letter: " << StringCheck.at(0) << std::endl;
	std::cout << "Last letter: " << StringCheck.back() << std::endl;
}